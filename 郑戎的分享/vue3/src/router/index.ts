import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

const history = createWebHashHistory()
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Main',
    component: () => import('../views/Main.vue'),
    children: [
      {
        path: '/ad',
        name: 'Ad',
        component: () => import('../views/Ad/index.vue')
      },
      {
        path: '/document',
        name: 'Document',
        component: () => import('../views/Document/')
      },
      {
        path: '/cli',
        name: 'Cli',
        component: () => import('../views/Cli/index.vue')
      },
      {
        path: '/vuecli',
        name: 'VueCli',
        component: ()=> import('../views/Start/VueCli.vue')
      },
      {
        path: '/create',
        name: 'Create',
        component: ()=> import('../views/Start/Create.vue')
      },
      {
        path: '/config',
        name: 'Config',
        component: ()=> import('../views/Start/Config.vue')
      },
      {
        path: '/structure',
        name: 'Structure',
        component: ()=> import('../views/Composition/Structure.vue')
      },
      {
        path: '/setup',
        name: 'Setup',
        component: ()=> import('../views/Composition/Setup.vue')
      }
    ]
  },
]

const router = createRouter({
  history,
  routes
})

export default router
