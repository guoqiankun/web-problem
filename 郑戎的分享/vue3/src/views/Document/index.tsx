import { defineComponent } from 'vue';
import './index.less'

const Document = defineComponent({
  name: 'Document',
  setup() {
    return () => (
      <>
        <iframe src="https://vue3js.cn/docs/zh/guide/introduction.html" frameborder="0" class="document-iframe"></iframe>
      </>
    )
  }
})

export default Document