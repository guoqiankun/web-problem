import { createStore } from 'vuex'

export default createStore({
  state: {
    PACKUP: false
  },
  mutations: {
    changePackup(state, payload) {
      state.PACKUP = payload.packup
    }
  },
  actions: {
  },
  modules: {
  }
})
