# Vue3与TSX
## 涉及到的主要依赖
1. `vite@1.0.0-beta.11`：新一代脚手架
2. `vue@3.0.0-beta.22`：beta版
3. `vuex@4.0.0-beta.4`
4. `vue-router@4.0.0-beta.2`
5. `typescript@3.9.6`

## 准备工作
1. 确保安装`yarn`
```bash
npm install yarn -g
```
2. 确保安装`vite`脚手架
```bash
npm install -g create-vite-app
# or
yarn add -g create-vite-app
```

## 开始
### 项目初始化
```bash
yarn create vite-app <project-name>
```

### 集成TS
```bash
yarn add --dev typescript
```
项目根目录创建配置文件：`tsconfig.json`：
```js
{
  "include": ["./**/*.ts"],
  "compilerOptions": {
    "jsx": "react",
    "target": "es2020" /* Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017','ES2018' or 'ESNEXT'. */,
    "module": "commonjs" /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */,
    // "lib": ["es2017.object"] /* Specify library files to be included in the compilation. */,
    // "declaration": true /* Generates corresponding '.d.ts' file. */,
    // "declarationMap": true,                /* Generates a sourcemap for each corresponding '.d.ts' file. */
    "sourceMap": true /* Generates corresponding '.map' file. */,
    // "outFile": "./",                       /* Concatenate and emit output to single file. */
    "outDir": "./dist" /* Redirect output structure to the directory. */,

    "strict": true /* Enable all strict type-checking options. */,
    "noUnusedLocals": true /* Report errors on unused locals. */,
    "noImplicitReturns": true /* Report error when not all code paths in function return a value. */,

    "moduleResolution": "node" /* Specify module resolution strategy: 'node' (Node.js) or 'classic' (TypeScript pre-1.6). */,
    "esModuleInterop": true /* Enables emit interoperability between CommonJS and ES Modules via creation of namespace objects for all imports. Implies 'allowSyntheticDefaultImports'. */
  }
}
```

到这一步，一个Vue3+TSX的项目就搭建起来了，以上配置文件的具体内容就不做解释了。

### 优化TS类型推断
`source.d.ts`: (优化编译器提示，声明静态资源文件)
```ts
declare const React: string;
declare module '*.json';
declare module '*.png';
declare module '*.jpg';
```
### main.ts
最终main.ts中引入store、router：
```typescript
import { createApp } from 'vue';
import App from './App';
import router from './router';
import store from './store';

createApp(App).use(router).use(store).mount('#app');
```  
### TSX
最终我们的组件代码，都会是这样的：`App.tsx`:  
```tsx
import { defineComponent } from 'vue';
import { RouterLink, RouterView } from 'vue-router';
import './style/main.scss'

export default defineComponent({
  name: 'App',
  setup() {
    return () => (
      <>
        <div id="nav">
          <RouterLink to="/">Home</RouterLink> |
          <RouterLink to="/about">About</RouterLink>
        </div>
        <RouterView/>
      </>
    );
  }
});
```  