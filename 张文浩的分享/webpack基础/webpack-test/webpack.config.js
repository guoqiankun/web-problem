const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports={
    entry: './src/js/index.js',
    output: {
        path: path.resolve(__dirname,'dist'),
        filename: '[name][hash:5].js'
    },
    module: {
        rules: [
            {
                test: /\.css/,
                include: path.resolve(__dirname, 'src'),
                exclude: /node_modules/,
                use: [{
                    loader: MiniCssExtractPlugin.loader
                }, 'css-loader']
            },
            {
                test:/\.(jpg|png|bmp|gif|svg|ttf|woff|woff2|eot)/,
                use:[
                    {
                        loader:'url-loader',
                        options: {
                            limit: 4000,
                            outputPath: '/img'
                        }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: 'html-withimg-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html',
            minify: {
                removeAttributeQuotes: true
            }
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name][hash:5].css'
        })
    ],
    devServer: {
        contentBase :path.resolve(__dirname,'dist'),
        host: 'localhost',
        port: 8080
    }
}
