import Vue from 'vue'
import Router from 'vue-router'
import myeditor from './../view/myeditor.vue'
import wangEditor from './../view/wangEditor.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/myeditor',
      name: 'myeditor',
      component: myeditor
    },
    {
      path: '/wangEditor',
      name: 'wangEditor',
      component: wangEditor
    },{
      path: '*',
      name: 'wangEditor',
      component: wangEditor
    }
  ]
})
