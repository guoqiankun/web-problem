### 几个没什么用的chrome开发工具小芝士



##### 1. 选取dom元素

   1. 常规的可以在控制台通过=\$选取DOM。相当于原生JS的document.querySelector方法。这个方法返回第一个匹配选择规则的DOM元素。
      也可以通过\$\$(tagName)和\$\$('.className')返回匹配的所有DOM元素。返回值是一个数组。相当于原生的document.querySelectorAll方法
   2. 可以使用$0快速定位到当前选中元素
      \$0 - \$4依次返回5个最近在元素面板选择过的DOM元素的历史记录，\$0是最新记录，依次类推
   3. Store as global variable
     1. 首先在elements面板中，选择一个DOM元素
     2. 点击右键选择「Store as global variable」
     3. 这时候console中就会出现相对应的元素，对应变量名为`templ`,`temp2`,`temp3`以此类推
      ![Shortcuts to access the DOM in JavaScript from the Console Panel](https://umaar.com/assets/images/dev-tips/store-global-dom-tree.gif)
[store global dom tree](https://umaar.com/dev-tips/183-store-global-dom-tree/)
	
##### 2. See affected nodes and matching selectors with long hover
	在elements面板中的styles面板下，将鼠标移放到对应属性值上（如margin、padding）几秒，可以看到页面上受到这些属性值影响的元素区域。

![See affected nodes and matching selectors with long hover](https://umaar.com/assets/images/dev-tips/css-hover.gif)

[css-hover](https://umaar.com/dev-tips/198-css-hover/)

##### 3. 更方便的css输入 better css presets for quick prototyping
有时候为了看一些效果的时候，可能会直接在网页上直接添加一些简单的样式。但碰到一些复杂的样式的时候，比如渐变啊什么的时候，想不起来函数写法可能还要另开一个页面搜索。浏览器并不会自动补全。
现在，如果想要在页面上调试，输入background,lin就会自动补全
`linear-gradient(45deg, #000, #0000)`

下面是一些别的预设属性值
filter: op -> filter: opacity(0.5)
filter: b -> filter: blur(1px)
transform: sc -> transform: scale(1.5)
transform: ma -> transform: matrix(1, 0, 0, 1, 0, 0)
transform: scale3 -> transform: scale3d(1.5, 1.5, 1.5)
background-image: rep -> background-image: repeating-linear-gradient(45deg, #000, #0000 100px)

![Better CSS presets for quick prototyping](https://umaar.com/assets/images/dev-tips/css-autocomplete-preset.gif)

[css presets](https://umaar.com/dev-tips/196-css-autocomplete-preset/)

##### 4. 快速折叠代码块提高可读性
1. 在source面板下，选择要浏览的文件
2. 通过Cmd + Shift + P，然后搜索 folding
![foding](https://umaar.com/assets/images/dev-tips/code-folding.gif)
[folding](https://umaar.com/dev-tips/189-code-folding/)

在这个页面还可以干很多其他事情，比如截图
Capture full size screenshot
Capture screenshot
Capture node screenshot

![Capture full sized screenshots without a browser extension](https://umaar.com/assets/images/dev-tips/screenshot-capture.gif)

或者通过clear site data清理页面信息，如cookies、localstorage、cache storage、Web SQL



##### 5. 查看未使用过的css

​	![Export your raw Code Coverage Data](https://umaar.com/assets/images/dev-tips/code-coverage-export.gif)
​	1. 在console面板下，点击左边的三个点，选择「Coverage」
​	2. 选择`Start Instrumenting coverage and reload Page`
​	3. 点击「Expoert」选择导出
[coverage](https://umaar.com/dev-tips/187-code-coverage-export/)

##### 6. 快速展开所有Dom节点

![Multiple techniques to expand all child nodes in the DOM tree](https://umaar.com/assets/images/dev-tips/expand-nodes.gif)

	 1. 在elements面板下选择要展开的Dom节点，右键点击选择Expand recursively
	 2. 长按option，点击要展开的Dom节点（Windows下，Control + Alt）

##### 7. 优化分析

	1. Perfomance 面板
	2. Audits 面板
	3. Console Violation 提示代码中一些不规范、需要改进的地方。
![Instant performance suggestions with Console Violations](https://umaar.com/assets/images/dev-tips/console-violations.gif)

使用方法
	
	1. 在console面板下，点击'Default levels'下拉菜单，选择'Verbose'
	2. 在console面板下，输入violation

用途

	1. JavaScript which forced a reflow
	2. JavaScript which used `docuemnt.write`
	3. Slow executing `setTimeout` handlers
	4. Inefficient event listeners
	5. And many other violations


[console violation](https://umaar.com/dev-tips/192-console-violations/)





[tips](https://umaar.com/dev-tips/)

