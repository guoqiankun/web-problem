const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const { useCapture } = require('./main-process/capture-main');

let win;
function createWindow() {
    win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
    useCapture(win);

    win.loadFile(path.join(__dirname, './renderer-process/index.html'));
    win.webContents.openDevTools();

    win.on('closed', () => {
        win = null;
    });

    ipcMain.on('hide-capture-main', () => {
        win.hide();
    });
    ipcMain.on('show-capture-main', () => {
        win.show();
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});