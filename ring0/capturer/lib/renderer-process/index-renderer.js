const { ipcRenderer, remote, desktopCapturer } = require('electron');
const $ = require('jquery');
const $jsCaptrue = $('#js-capture');

$jsCaptrue.on('click', () => {
    // 创建全屏窗口，加载capture.html，获取屏幕截图插入dom
    let val = $('input[name=type]:checked').val();
    ipcRenderer.send('capture-screen', {
        typeVal: val
    });
});

ipcRenderer.on('capture-pic-data', (e, dataUrl) => {
    console.log(dataUrl);
    let $img = document.createElement('img');
    $img.src = dataUrl;
    document.body.appendChild($img); 
});

$("#xxx").on('click', () => {
    console.time("1");
    desktopCapturer.getSources({types: ['screen']}).then(async (sources) => {
        for (let i = 0; i < sources.length; ++i) {
            navigator.mediaDevices.getUserMedia({
                audio: false,
                video: {
                    mandatory: {
                        chromeMediaSource: 'desktop',
                        chromeMediaSourceId: sources[i].id,
                        minWidth: 1280,
                        maxWidth: 1280,
                        minHeight: 720,
                        maxHeight: 720
                    }
                }
            })
            .then((stream) => handleStream(stream))
            .catch((e) => handleError(e))
        }
    })

    function handleStream (stream) {
        const video = document.createElement('video');
        video.style.cssText = 'width: 200px; height: 200px;';
        video.srcObject = stream;
        document.body.appendChild(video);
        video.onloadedmetadata = (e) => video.play()
        console.timeEnd("1");
    }

    function handleError (e) {
        console.log(e)
    }
});