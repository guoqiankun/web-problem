const { BrowserWindow, screen, ipcMain, globalShortcut } = require('electron');
const os = require('os');
const path = require('path');

let captureWins = [];
let mainWins = null;

ipcMain.on('show-capture-renderer', (e, display_id = '') => {
    captureWins.forEach(it => {
        if (it.display_id.toString() === display_id.toString()) {
            it.show();
        }
    });
});

ipcMain.on('show-capture-renderer-all', (e) => {
    captureWins.forEach(it => {
        it.show();
    });
});


const captureScreen = (e, args) => {
    console.time('创建透明窗口');
    console.log(args);
    if (captureWins.length) {
        return
    }

    let displays = screen.getAllDisplays()
    captureWins = displays.map((display) => {
        let captureWin = new BrowserWindow({
            // window 使用 fullscreen,  mac 设置为 undefined, 不可为 false
            fullscreen: os.platform() === 'win32' || undefined,
            width: display.size.width,
            height: display.size.height,
            x: display.bounds.x,
            y: display.bounds.y,
            transparent: true,
            frame: false,
            skipTaskbar: true,
            autoHideMenuBar: true, 
            movable: false, 
            resizable: false,
            enableLargerThanScreen: true,
            hasShadow: false,
            webPreferences: {
                webSecurity: false,
                nodeIntegration: true
            },
            show: false
        })
        captureWin.setAlwaysOnTop(true, 'screen-saver')
        captureWin.setVisibleOnAllWorkspaces(true)
        captureWin.setFullScreenable(false)

        captureWin.loadFile(path.join(__dirname, '../renderer-process/capture.html'), {
            query: {
                width: display.size.width,
                height: display.size.height,
                display_id: display.id,
                type_val: args.typeVal || 1
            }
        });
        captureWin.openDevTools()

        captureWin.on('closed', () => {
            let index = captureWins.indexOf(captureWin)
            if (index !== -1) {
                captureWins.splice(index, 1)
            }
            captureWins.forEach(win => win.close())
        });
        captureWin.display_id = display.id;
        return captureWin
    });
    console.timeEnd('创建透明窗口');
};

const closeCaptureWin = (win) => {
    if (captureWins) {
        captureWins.forEach(win => win.close());
        captureWins = [];
        win.show();
    }
};

const useCapture = (win) => {
    mainWins = win;
    globalShortcut.register('Esc', () => {
        closeCaptureWin(win);
    });

    globalShortcut.register('CmdOrCtrl+Shift+A', (e) => {
        win.hide();
        captureScreen(e);
    });

    ipcMain.on('capture-close', (e) => {
        closeCaptureWin(win);
    });

    ipcMain.on('capture-screen', (e, { type = 'start', screenId, url, typeVal = 1 } = {}) => {
        console.log(type);
        if (type === 'start') {
            win.hide();
            captureScreen(e, {
                typeVal
            });
        } else if (type === 'complete') {
            closeCaptureWin(win);
            win.webContents.send('capture-pic-data', url);
        } else if (type === 'select') {
            captureWins.forEach(win => win.webContents.send('capture-screen', { type: 'select', screenId }));
        }
    });
};

exports.closeCaptureWin = closeCaptureWin;
exports.useCapture = useCapture;
exports.captureSceen = captureScreen;
