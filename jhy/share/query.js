const querystring = require('querystring');
const http = require('http');
const fs = require('fs');

let postData = querystring.stringify({
    'method': 'release',
    'version': '1.0.1',
    'localName': 'pdd-discount-web-web',
    'remotePackageVersion': '201905/pdd-discount-web-web/20190507_195925^pdd-discount-web-web^1.0.1.zip.encrypt.zip',
});
// method='dasd'$
let options = {
    hostname: 'localhost',
    port: 2200,
    path: '/api',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(postData)
    }
};

const req = http.request(options, (res) => {
    console.log(`STATUS: ${res.statusCode}`);
    console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
        console.log(`BODY: ${chunk}`);

    });
    res.on('end', () => {
        console.log('No more data in response.');
    });
});

req.on('error', (e) => {
    console.error(111, e);
    console.error(`problem with request1: ${e.message}`);
});

// Write data to request body
req.write(postData);

console.log('postData', postData)
req.end();
console.log('end');
