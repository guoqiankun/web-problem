const Koa = require('koa');

const koaBody = require('koa-body')

const bodyParser = require('koa-bodyparser');

var cors = require('koa-cors');

const app = new Koa();

const router = require('./router');

app.use(bodyParser());

app.use(cors());

app.use(koaBody({
    multipart: true,
    formidable: {
        maxFileSize: 2000 * 1024 * 1024    // 设置上传文件大小最大限制，默认20M
    }
}));

app.use(async (ctx, next) => {
    console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    await next();
});
// add router middleware:
app.use(router.routes())
    .use(router.allowedMethods());;

let port = 2200;
app.listen(port);
console.log('app started at port ' + port + '...');





















