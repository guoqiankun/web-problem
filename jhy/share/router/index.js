'use strict';

let router = require('koa-router')();


var multer = require('multer');

const path = require('path');

const fs = require('fs');

router.get('/', async (ctx, next) => {
    ctx.response.body = `<form action="/uploadfile" method="post" enctype="multipart/form-data">
	    <input type="file" name="uploadFile" id="file" value="" multiple="multiple" />
	    <input type="submit" value="提交"/>
	</form>`;
});

router.get('/upload', async (ctx, next) => {
    ctx.response.body = `
    	<script src="//staticnew.superboss.cc/lib/jquery.js"></script>
    	<h1>Index</h1>
			<p>Name: <input name="packgeBranch" value="test"></p>
			<p>Name: <input name="packgeParameter" value="production"></p>
			<p>Name: <input name="localName" value="pdd-discount-web-web"></p>
			<p>Name: <input name="packgeVersion" value="1.0.1"></p>
			<p>Name: <input name="userAuthToken" value="44EF2548C85C79FE19147EBAF9234879"></p>
			<p>Name: <input name="packgeUrl" value="git@git2.superboss.cc:raycloudFrontEnd/superboss-pdd-promotionalTools.git"></p>
	        <p>Password: <input type="file" class="J_uploadFile" name="uploadFile" multiple /></p>
	        <p><input class="J_submit" type="submit" value="Submit"></p>

	        <script>

	        	$('.J_submit').click(function(e){
		        	var formData = new FormData();
		        	formData.append("packgeBranch", 'test');
		        	formData.append("packgeParameter", 'production');
		        	formData.append("localName", 'pdd-discount-web-web');
		        	formData.append("packgeVersion", '1.0.1');
		        	formData.append("userAuthToken", '44EF2548C85C79FE19147EBAF9234879');
		        	formData.append("packgeUrl", 'git@git2.superboss.cc:raycloudFrontEnd/superboss-pdd-promotionalTools.git');
	                formData.append("file", $('.J_uploadFile')[0].files[0]);

	        		$.ajax({
						url: '/uploadfile',
						data: formData,
						type: 'POST',
						processData: false,
						contentType: false,
						success: function(json){
							console.log(json);
						},error: function(err){
							console.log(err);
						}
					})
	        	});
	        </script>
    `;
});

router.post('/uploadfile', async (ctx, next) => {
    console.log(ctx)
    // 上传单个文件
    const file = ctx.request.files.file; // 获取上传文件

    console.log('-----------11111111----------');
    console.log('file', file)
    console.log('file.path', file.path)
    fs.writeFile('file.path.txt', file.path, function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log('ok.');
        }
    });

    // 创建可读流
    const reader = fs.createReadStream(file.path);

    console.log('-----------2222222222----------');
    console.log('reader', reader)

    let filePath = path.join(__dirname, '../public') + `/${file.name}`;

    console.log('-----------333333333----------');
    console.error('filePath', filePath);

    // 创建可写流
    const upStream = fs.createWriteStream(filePath);

    console.log('-----------44444444444----------');
    console.log('upStream', upStream);

    // 可读流通过管道写入可写流
    reader.pipe(upStream);

    console.log('-----------555555----------');

    return ctx.body = "上传成功！";
});

router.post('/api', async (ctx, next) => {
    console.log(ctx.req);
    console.log(ctx.res);
    ctx.response.body = `111`
});

// require('./index')(router);
require('./cors')(router);

module.exports = router;