'use strict';

let router = require('koa-router')();

module.exports = function (router) {
    router.get('/cors', async (ctx, next) => {
        ctx.response.body = `<form action="/jsons" method="post" enctype="multipart/form-data">
            <input type="file" name="file" id="file" value="" multiple="multiple" />
            <input type="submit" value="提交"/>
        </form>`;
    });

    router.get('/jsons', async (ctx, next) => {
        return ctx.body = "上传成功！";
    });
};